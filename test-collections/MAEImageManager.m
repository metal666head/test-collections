//
//  MAEImageManager.m
//  test-collections
//
//  Created by Anton Marunko on 08/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import "MAEImageManager.h"
#import "MAEImageCache.h"
#import "MAEImageDownloader.h"
#import "objc/runtime.h"

@interface MAEImageManager ()

@property (nonatomic, strong) MAEImageCache *inMemoryCache;
@property (nonatomic, strong) MAEImageDownloader *imageDownloader;

@end

@implementation MAEImageManager

+ (nonnull instancetype)sharedImageManager {
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    return instance;
}

#pragma mark - Init

- (instancetype)init {
    if(self = [super init]){
        _inMemoryCache = [MAEImageCache sharedImageCache];
        _imageDownloader = [MAEImageDownloader sharedDownloader];
    }
    
    return self;
}

#pragma mark - Custom

- (void)imageWithURL:(NSURL *)url completionBlock:(void(^)(UIImage * image))completionBlock{
    UIImage * image = [self.inMemoryCache imageFromMemoryInCacheForKey:[url absoluteString]];
    if(image){
        completionBlock(image);
    }else{
        [self.imageDownloader downloadImageWithURL:url completionBlock:completionBlock];
    }
}

@end
