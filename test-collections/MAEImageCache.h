//
//  MAEFileCache.h
//  test-collections
//
//  Created by Anton Marunko on 08/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MAEImageCache : NSObject

+ (instancetype)sharedImageCache;
- (void)saveImage:(UIImage *)image forKey:(NSString *)key;
- (UIImage *)imageFromMemoryInCacheForKey:(NSString *)key;

@end
