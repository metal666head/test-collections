//
//  ViewController.m
//  test-collections
//
//  Created by Anton Marunko on 06/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import "CollectionViewController.h"
#import "MAECollectionViewDataSource.h"
#import "MAEPictureCollectionViewCell.h"
#import "MAECollectionViewFlowLayout.h"

static const CGFloat kSpacingForCollectionView = 10.0;
static const float kMinimalRefreshControlSystemVersion = 10.0;

@interface CollectionViewController ()

@property (nonatomic, strong) MAECollectionViewDataSource * collectionDataSource;
@property (nonatomic, strong) MAECollectionViewFlowLayout * flowLayout;
@property (nonatomic, strong) UICollectionView * picturesCollectionView;
@property (nonatomic, assign) CGFloat cellheight;

@end

@implementation CollectionViewController

#pragma mark - LifeCycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupCollectionView];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    self.picturesCollectionView.frame = self.view.frame;
    self.cellheight = [self.picturesCollectionView bounds].size.width - kSpacingForCollectionView * 2;
    self.flowLayout.itemSize = CGSizeMake(self.cellheight, self.cellheight);
    [self.picturesCollectionView reloadData];
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Setup 

- (void)setupCollectionView{
    self.cellheight = self.view.frame.size.width - kSpacingForCollectionView * 2;
    self.picturesCollectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:[self coollectionViewlayout]];
    
    if([[UIDevice currentDevice].systemVersion floatValue] >= kMinimalRefreshControlSystemVersion){
        self.picturesCollectionView.refreshControl = [UIRefreshControl new];
        [self.picturesCollectionView.refreshControl addTarget:self action:@selector(refreshCollectionView:) forControlEvents:UIControlEventValueChanged];
    }else{
        UIRefreshControl *refreshControl = [UIRefreshControl new];
        [refreshControl addTarget:self action:@selector(refreshCollectionView:)
                 forControlEvents:UIControlEventValueChanged];
        [self.picturesCollectionView addSubview:refreshControl];
        self.picturesCollectionView.alwaysBounceVertical = YES;
    }
    
    self.collectionDataSource = [MAECollectionViewDataSource new];
    self.picturesCollectionView.dataSource = self.collectionDataSource;
    self.picturesCollectionView.delegate = self;
    self.picturesCollectionView.backgroundColor = [UIColor whiteColor];
    [self.picturesCollectionView registerClass:[MAEPictureCollectionViewCell class] forCellWithReuseIdentifier:kCellReuseIdentifier];
    
    [self.view addSubview:self.picturesCollectionView];
}

- (UICollectionViewFlowLayout *)coollectionViewlayout{
    self.flowLayout = [MAECollectionViewFlowLayout new];
    self.flowLayout.sectionInset = UIEdgeInsetsMake(kSpacingForCollectionView, kSpacingForCollectionView, kSpacingForCollectionView, kSpacingForCollectionView);
    [self.flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    self.flowLayout.itemSize = CGSizeMake(self.cellheight, self.cellheight);
    return self.flowLayout;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger index = [indexPath row];
    
    [self.collectionDataSource removeDataAtIndex:index];
    [self.picturesCollectionView deleteItemsAtIndexPaths:@[indexPath]];
}

#pragma mark - Actions

- (void)refreshCollectionView:(id)sender {
    [self.collectionDataSource reloadData];
    [self.picturesCollectionView reloadData];
    if([[UIDevice currentDevice].systemVersion floatValue] >= kMinimalRefreshControlSystemVersion){
        [self.picturesCollectionView.refreshControl endRefreshing];
    }else{
        if([sender isKindOfClass:[UIRefreshControl class]]){
            [((UIRefreshControl *)sender) endRefreshing];
        }
    }
}

@end
