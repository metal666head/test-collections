//
//  UIImageView+UrlCache.h
//  test-collections
//
//  Created by Anton Marunko on 08/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (UrlCache)

- (void)setImageWithURL:(NSURL *)url;

@end
