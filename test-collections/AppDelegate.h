//
//  AppDelegate.h
//  test-collections
//
//  Created by Anton Marunko on 06/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

