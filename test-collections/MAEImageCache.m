//
//  MAEFileCache.m
//  test-collections
//
//  Created by Anton Marunko on 08/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import "MAEImageCache.h"

@interface MAEImageCache ()

@property (nonatomic, strong) NSCache *memoryInCache;

@end

@implementation MAEImageCache

+ (instancetype)sharedImageCache{
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    return instance;
}

#pragma mark - Init

- (instancetype)init{
    if (self = [super init]) {
        _memoryInCache = [NSCache new];
    }
    
    return self;
}

#pragma mark - Custom

- (CGFloat)imageCost:(UIImage *)image{
    return image.size.height * image.size.width * image.scale * image.scale;
}

- (void)saveImage:(id)image forKey:(NSString *)key{
    NSUInteger cost = [self imageCost:image];
    [self.memoryInCache setObject:image forKey:key cost:cost];
}

- (UIImage *)imageFromMemoryInCacheForKey:(NSString *)key {
    return [self.memoryInCache objectForKey:key];
}

@end
