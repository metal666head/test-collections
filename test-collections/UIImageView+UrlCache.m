//
//  UIImageView+UrlCache.m
//  test-collections
//
//  Created by Anton Marunko on 08/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import "UIImageView+UrlCache.h"
#import "MAEImageManager.h"

@implementation UIImageView (UrlCache)

- (void)setImageWithURL:(NSURL *)url{
    if(url){
        
        __weak __typeof(self)wself = self;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong __typeof (wself) sself = wself;
            [[MAEImageManager sharedImageManager] imageWithURL:url completionBlock:^(UIImage *image) {
                if (!sself)
                    return;
                
                sself.image = image;
                [sself setNeedsLayout];
            }];
        });
    }
}

@end
