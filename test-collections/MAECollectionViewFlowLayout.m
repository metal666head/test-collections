//
//  MAECollectionViewFlowLayout.m
//  test-collections
//
//  Created by Anton Marunko on 09/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import "MAECollectionViewFlowLayout.h"

static const CGFloat kFinalAlpha = 0.5;

@implementation MAECollectionViewFlowLayout

- (UICollectionViewLayoutAttributes *)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath{
    
    UICollectionViewLayoutAttributes *attributes = [super finalLayoutAttributesForDisappearingItemAtIndexPath:itemIndexPath];
    
    CGFloat endX = CGRectGetWidth(self.collectionView.bounds);

    CATransform3D endTransform = CATransform3DMakeTranslation(endX, 0, 0);
    attributes.transform3D = endTransform;
    
    attributes.alpha = kFinalAlpha;
    return attributes;
}

@end
