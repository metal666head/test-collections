//
//  MAEImageDownloader.m
//  test-collections
//
//  Created by Anton Marunko on 08/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import "MAEImageDownloader.h"
#import "MAEImageCache.h"

@interface MAEImageDownloader ()

@property (nonatomic, strong) NSURLSession * session;

@end

@implementation MAEImageDownloader

+ (instancetype)sharedDownloader {
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    return instance;
}

- (instancetype)init {
    if(self = [super init]){
        self.session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                     delegate:nil
                                                delegateQueue:nil];
    }
    return self;
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void(^)(UIImage * image))completionBlock{
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSURLSessionDataTask * sessionTask = [self.session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                if(data){
                    
                    UIImage * image = [UIImage imageWithData:data];
                    
                    if(image){
                        [[MAEImageCache sharedImageCache] saveImage:image forKey:[url absoluteString]];
                    }
                    
                    if(completionBlock){
                        completionBlock(image);
                    }
                }
            }
        });
    }];
    
    [sessionTask resume];
}

@end
