//
//  MAEPictureCollectionViewCell.m
//  test-collections
//
//  Created by Anton Marunko on 08/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import "MAEPictureCollectionViewCell.h"

@implementation MAEPictureCollectionViewCell

#pragma mark - Init

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
        self.imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        [self.contentView addSubview:self.imageView];
    }
    
    return self;
}

@end
