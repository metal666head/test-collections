//
//  MAECollectionViewDataSource.h
//  test-collections
//
//  Created by Anton Marunko on 08/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define kCellReuseIdentifier @"collectionCell"

@interface MAECollectionViewDataSource : NSObject <UICollectionViewDataSource>

- (void)reloadData;
- (void)removeDataAtIndex:(NSInteger)index;

@end
