//
//  MAECollectionViewDataSource.m
//  test-collections
//
//  Created by Anton Marunko on 08/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import "MAECollectionViewDataSource.h"
#import "MAEPictureCollectionViewCell.h"
#import "UIImageView+UrlCache.h"

#define kDataPlistFileName @"Pictures.plist"

@interface MAECollectionViewDataSource ()

@property (nonatomic, strong) NSMutableArray * data;

@end

@implementation MAECollectionViewDataSource

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MAEPictureCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier forIndexPath:indexPath];
    
    NSInteger index = [indexPath row];
    NSURL * url = [NSURL URLWithString:self.data[index]];
    
    [cell.imageView setImageWithURL:url];
    
    return cell;
}

#pragma mark - Setters/Getters

- (NSMutableArray *)data{
    if(_data){
        return _data;
    }
    else{
        [self reloadData];
        return _data;
    }
}

#pragma mark - Managing Data

- (void)reloadData{
    _data = [[NSMutableArray alloc] initWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:kDataPlistFileName]];
}

- (void)removeDataAtIndex:(NSInteger)index{
    [self.data removeObjectAtIndex:index];
}

@end
