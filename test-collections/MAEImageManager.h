//
//  MAEImageManager.h
//  test-collections
//
//  Created by Anton Marunko on 08/02/2017.
//  Copyright © 2017 orgName. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MAEImageManager : NSObject

+ (instancetype)sharedImageManager;

- (void)imageWithURL:(NSURL *)url completionBlock:(void(^)(UIImage * image))completionBlock;

@end
